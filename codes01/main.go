package main

import ( "net/http"
		 "time" 
		 "log")

func main() {

	StartServer()
	
	// Essa linha deve ser executada sem alteração
	// da função StartServer	
	log.Println("[INFO] Servidor no ar!")
}

func RequestPrint(w http.ResponseWriter, r *http.Request) {
	url := r.URL.String()
	met := r.Method

	w.Write([]byte( met + " " + url))
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.122:8082",
			IdleTimeout: duration, 
	}

	http.HandleFunc("/", RequestPrint)

	log.Print(server.ListenAndServe())
}